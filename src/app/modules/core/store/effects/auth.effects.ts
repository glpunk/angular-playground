import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { filter, tap, mapTo } from 'rxjs/operators';
import * as authActions from '@store/actions/auth.actions';

@Injectable()
export class AuthEffects {

  isAuthenticatedAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.AuthisAuthenticatedAction),
      filter(auth => !auth.isAuthenticated),
      tap(() => this.router.navigate(['/login'])),
      mapTo(authActions.AuthRedirectAction())
    )
  );

  constructor(
    private actions$: Actions,
    private router: Router
  ) { }
}
