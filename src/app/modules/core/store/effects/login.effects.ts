import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import * as loginActions from '@store/actions/login.actions';
import { LoginService } from '@core/services';

@Injectable()
export class LoginEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginActions.SubmitFormAction),
      exhaustMap(action =>
        this.loginService.login(action.credentials).pipe(
          map(auth => loginActions.LoginSuccessAction({ auth })),
          catchError(error => of(loginActions.LoginFailureAction({ error })))
        )
      )
    )
  );

  authenticated$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginActions.LoginSuccessAction),
      map(() => {
        this.router.navigate(['/']);
        return loginActions.LoginRedirectAction();
      })
    )
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private loginService: LoginService
  ) { }
}
