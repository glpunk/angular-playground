import {
  Action,
  createReducer,
  createSelector,
  createFeatureSelector,
  on
} from '@ngrx/store';

import {
  LoginSuccessAction,
  LoginFailureAction
} from '@store/actions/login.actions';

export const LoginFeatureKey = 'login';

export interface State {
  status: boolean;
  jwt: string;
  user: object;
}

export const initialState: State = {
  status: null,
  jwt: null,
  user: null
};

const reducer = createReducer(
  initialState,
  on(
    LoginSuccessAction,
    (_, { auth }) => ({ status: true, jwt: auth.jwt, user: auth.user })
  ),
  on(
    LoginFailureAction,
    (_, { error }) => ({ status: false, jwt: null, user: null })
  )
);

export function LoginReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}

// Selectors
export const SelectLoginState = createFeatureSelector<State>(LoginFeatureKey);

export const GetLoginStatus = (state: State) => state.status;
export const GetJwt = (state: State) => state.jwt;

export const SelectLoginStatusState = createSelector(
  SelectLoginState,
  GetLoginStatus
);

export const SelectJwtState = createSelector(
  SelectLoginState,
  GetJwt
);
