import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { LoginReducer } from './login.reducer';
import * as fromLogin from './login.reducer';
import * as actions from '@store/actions/login.actions';
import { Auth } from '@core/models';
import { Subscription } from 'rxjs';

describe('LoginReducer', () => {
  let mockStore: MockStore<fromLogin.State>;

  const expectedResult: Auth = {
    jwt: '1234qwerty',
    user: {
      id: 1,
      username: 'mockuser'
    }
  };

  const initialState = {
    login: {
      status: false,
      jwt: null,
      user: null
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })],
      declarations: [],
    });

    mockStore = TestBed.inject(Store as any);
  });

  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = LoginReducer(undefined, action);
      expect(result).toEqual(fromLogin.initialState);
    });
  });
  describe('on LoginActions.LoginSuccessAction', () => {
    it('sets status to true', () => {
      const action = actions.LoginSuccessAction;
      const result = LoginReducer(fromLogin.initialState, action({ auth: expectedResult }));
      expect(result.status).toEqual(true);
    });
    it('sets jwt value', () => {
      const action = actions.LoginSuccessAction;
      const result = LoginReducer(fromLogin.initialState, action({ auth: expectedResult }));
      expect(result.jwt).toEqual('1234qwerty');
    });
    it('sets user value', () => {
      const action = actions.LoginSuccessAction;
      const result = LoginReducer(fromLogin.initialState, action({ auth: expectedResult }));
      expect(result.user).toEqual(expectedResult.user);
    });
  });
  describe('on LoginActions.LoginFailureAction', () => {
    it('sets state as initial state', () => {
      const action = actions.LoginFailureAction;
      const result = LoginReducer(fromLogin.initialState, action({ error: {} }));
      expect(result.status).toEqual(false);
    });
  });
  describe('Selectors', () => {
    describe('SelectLoginStatusState', () => {
      it('Returns status value', (done) => {
        const subs: Subscription = mockStore.select(fromLogin.SelectLoginStatusState).subscribe(
          result => {
            expect(result).toEqual(false);
            done();
          }
        );
      });
    });
  });
});
