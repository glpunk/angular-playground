import { createAction, props } from '@ngrx/store';
import { Auth, Credentials } from '@core/models';

export const SubmitFormAction = createAction('[Login Component] submitForm',
props<{credentials: Credentials}>());

export const LoginSuccessAction = createAction('[Login Component] loginSuccess',
props<{auth: Auth}>());

export const LoginRedirectAction = createAction('[Login Component] loginRedirect');

export const LoginFailureAction = createAction('[Login Component] loginFailure',
props<{error: any}>());


