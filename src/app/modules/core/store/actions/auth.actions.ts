import { createAction, props } from '@ngrx/store';

export const AuthRedirectAction = createAction('[Auth] AuthRedirect');

export const AuthisAuthenticatedAction = createAction('[Auth] AuthisAuthenticated',
props<{isAuthenticated: boolean}>());


