export interface Auth {
  jwt: string;
  user: object;
}
