import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { SelectLoginStatusState } from '@store/reducers/login.reducer';
import { Observable } from 'rxjs';

@Injectable()
export class AuthService {
  loginState$: Observable<boolean> = this.store.select(SelectLoginStatusState);
  public isAuthenticated = false;

  constructor( private store: Store) {
    this.loginState$.subscribe(result => this.isAuthenticated = result);
  }
}
