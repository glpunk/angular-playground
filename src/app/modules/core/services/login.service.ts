import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Auth, Credentials } from '@core/models';

@Injectable()
export class LoginService {
  private API_PATH = 'http://localhost:3000/auth/local';

  constructor(
    private http: HttpClient
  ) { }

  login(credentials: Credentials): Observable<Auth> {
    return this.http.post<Auth>( `${this.API_PATH}/`, credentials)
    .pipe(map(result => result));
  }
}
