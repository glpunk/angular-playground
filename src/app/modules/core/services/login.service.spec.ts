import { TestBed } from '@angular/core/testing';
import { LoginService } from './login.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Auth, Credentials } from '@core/models';
import { defer } from 'rxjs';

describe('LoginService', () => {
  let service: LoginService;
  let httpClientSpy: { post: jasmine.Spy };

  const credentials: Credentials = {
    identifier: '',
    password: ''
  };

  function asyncData<T>(data: T) {
    return defer(() => Promise.resolve(data));
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [ HttpClient, LoginService ]
    });
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    service = new LoginService(httpClientSpy as any);
  });

  describe('Constructor', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });
  });
  it('returs expected Auth response (HttpClient called once)', () => {
    const expectedResponse: Auth = {
      jwt: '23oi4y2erwkdjfn',
      user: {
        id: 1,
        username: 'gil'
      }
    };

    httpClientSpy.post.and.returnValue(asyncData(expectedResponse));

    service.login(credentials).subscribe(
      heroes => expect(heroes).toEqual(expectedResponse)
    );
    expect(httpClientSpy.post.calls.count()).toBe(1);
  });
});
