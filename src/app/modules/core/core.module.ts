import { NgModule, Optional, SkipSelf } from '@angular/core';
import { AuthGuard } from './guards';
import { LoginService, AuthService } from './services';

@NgModule({
  providers: [ AuthGuard, LoginService, AuthService ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only'
      );
    }
  }
}
