import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '@app/modules/core/services/auth.service';
import { Store } from '@ngrx/store';
import { AuthisAuthenticatedAction } from '@store/actions/auth.actions';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private store: Store) {}

  canActivate(): boolean {
    const isAuthenticated = this.authService.isAuthenticated;
    this.store.dispatch(AuthisAuthenticatedAction({isAuthenticated}));
    return isAuthenticated;
  }
}
