import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Credentials } from '@core/models';
import { SubmitFormAction } from '@store/actions/login.actions';
import { SelectLoginStatusState } from '@store/reducers/login.reducer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginStatus$ = this.store.select(SelectLoginStatusState);

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<{credentials: Credentials}>
    ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      identifier: '',
      password: ''
    });
  }
  onSubmit() {
    const credentials = this.loginForm.value;
    this.store.dispatch(SubmitFormAction({credentials}));
  }
}
