import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, MemoizedSelector } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoginComponent } from './login.component';
import * as fromLogin from '@store/reducers/login.reducer';
import { SubmitFormAction } from '@store/actions/login.actions';
import { Credentials } from '@core/models';

import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let mockStore: MockStore<fromLogin.State>;
  let mockStateSelector: MemoizedSelector<fromLogin.State, boolean>;

  const initialState = {
    status: null,
    jwt: null,
    user: null
  };

  const credentials: Credentials = {
    identifier: '',
    password: ''
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [

      ],
      providers: [
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        FormBuilder,
        provideMockStore({ initialState })
      ],
      declarations: [LoginComponent]
    })
      .compileComponents();

    mockStore = TestBed.inject(Store as any);
    mockStateSelector = mockStore.overrideSelector(fromLogin.SelectLoginStatusState, false);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Constructor', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('onSubmit', () => {
    it('dispatches submitFormAction', () => {
      const store = fixture.debugElement.injector.get(Store);
      const spy = spyOn<any>(store, 'dispatch');
      component.onSubmit(credentials);
      expect(spy).toHaveBeenCalledWith(SubmitFormAction({ credentials }));
    });
  });
});
