# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Stub server
json-server dependency, more info: https://github.com/typicode/json-server

run:
```
npm run json-server
```

## references

* https://ngrx.io/docs
* https://ngrx.io/guide/store/testing
* https://github.com/blove/ngrx-testing
* https://github.com/ngrx/example-app
* https://duncanhunter.gitbook.io/testing-angular/
* https://indepth.dev/start-using-ngrx-effects-for-this/
* https://github.com/ngohungphuc/Research/tree/master/Angular/Redux/ReduxDemo
* https://www.apollographql.com/docs/angular/basics/queries/
* https://angular.io/tutorial
* https://jasonwatmore.com/post/2019/06/10/angular-8-user-registration-and-login-example-tutorial
* https://frontpills.com/posts/2019/core-shared-modules/
* https://itnext.io/* choosing-a-highly-scalable-folder-structure-in-angular-d987de65ec7
