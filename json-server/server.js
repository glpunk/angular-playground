const jsonServer = require('json-server')
const fs = require('fs')
const path = require('path')
const server = jsonServer.create()
const middlewares = jsonServer.defaults()

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares)
server.use(jsonServer.bodyParser)

server.post('/auth/local', (req, res) => {
  console.log(req.values);
  if (req.body.identifier === 'testuser' &&
    req.body.password === 'test') {
    res.status(200).send(
      fs.readFileSync(path.join(__dirname, 'auth/ok.json'))
    );
  } else {
    res.status(400).send(
      fs.readFileSync(path.join(__dirname, 'auth/fail.json'))
    );
  }
})

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser)
server.use((req, res, next) => {
  if (req.method === 'POST') {
    req.body.createdAt = Date.now()
  }
  // Continue to JSON Server router
  next()
})

server.listen(3000, () => {
  console.log('JSON Server is running')
})